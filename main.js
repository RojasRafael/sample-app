// main.js
async function fetchRandomJoke() {
  const response = await fetch('/random-joke');
  const jokeData = await response.json();
  document.getElementById('joke-container').innerText = jokeData.joke;
}

function navigateToAdd() {
  window.location.href = '/add';
}
