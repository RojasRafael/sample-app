CREATE DATABASE IF NOT EXISTS jokes_db;
USE jokes_db;
CREATE TABLE IF NOT EXISTS jokes_table (
    id INT AUTO_INCREMENT PRIMARY KEY,
    joke VARCHAR(350) NOT NULL
);

INSERT INTO jokes_table (joke)
VALUES
    ('Why don''t programmers like nature? It has too many bugs.'),
    ('How many programmers does it take to change a light bulb? None, that''s a hardware problem.'),
    ('Why do programmers prefer dark mode? Light attracts bugs.'),
    ('What''s a computer''s favorite beat? An algorithm.'),
    ('Why was the computer cold? It left its Windows open.'),
    ('Why do programmers always mix up Christmas and Halloween? Oct 31 == Dec 25.'),
    ('What''s a computer''s least favorite food? Spam.'),
    ('How do you comfort a JavaScript bug? You console it.'),
    ('Why did the computer go to therapy? It had too many bytes of emotional baggage.'),
    ('Why did the computer catch a cold? It left its Windows open.'),
    ('Why don''t programmers like nature? It has too many branches.'),
    ('How do you comfort a JavaScript bug? You console it.'),
    ('Why did the computer keep its drink on top of the modem? It wanted a data on the rocks.'),
    ('Why was the JavaScript developer sad? Because he didn''t "null" how to express himself.'),
    ('How do you stop a computer from getting too cold? You give it a byte.'),
    ('Why did the computer keep its drink on top of the modem? It wanted a data on the rocks.'),
    ('Why was the computer cold? It left its Windows open.'),
    ('How do you comfort a JavaScript bug? You console it.'),
    ('Why did the computer catch a cold? It left its Windows open.'),
    ('Why do programmers prefer dark mode? Light attracts bugs.'),
    ('How do you comfort a JavaScript bug? You console it.'),
    ('Why was the computer cold? It left its Windows open.'),
    ('Why did the computer go to therapy? It had too many bytes of emotional baggage.'),
    ('Why do programmers always mix up Christmas and Halloween? Oct 31 == Dec 25.'),
    ('What''s a computer''s least favorite food? Spam.'),
    ('How many programmers does it take to change a light bulb? None, that''s a hardware problem.'),
    ('Why was the JavaScript developer sad? Because he didn''t "null" how to express himself.'),
    ('How do you stop a computer from getting too cold? You give it a byte.'),
    ('Why did the computer keep its drink on top of the modem? It wanted a data on the rocks.'),
    ('Why was the JavaScript developer sad? Because he didn''t "null" how to express himself.'),
    ('How do you stop a computer from getting too cold? You give it a byte.'),
    ('Why do programmers prefer dark mode? Light attracts bugs.'),
    ('Why did the computer go to therapy? It had too many bytes of emotional baggage.'),
    ('How do you comfort a JavaScript bug? You console it.'),
    ('Why did the computer keep its drink on top of the modem? It wanted a data on the rocks.'),
    ('Why was the JavaScript developer sad? Because he didn''t "null" how to express himself.'),
    ('How many programmers does it take to change a light bulb? None, that''s a hardware problem.'),
    ('What''s a computer''s least favorite food? Spam.'),
    ('Why was the computer cold? It left its Windows open.'),
    ('How do you stop a computer from getting too cold? You give it a byte.'),
    ('Why did the computer go to therapy? It had too many bytes of emotional baggage.'),
    ('Why do programmers always mix up Christmas and Halloween? Oct 31 == Dec 25.'),
    ('How many programmers does it take to change a light bulb? None, that''s a hardware problem.'),
    ('What''s a computer''s least favorite food? Spam.'),
    ('Why was the computer cold? It left its Windows open.'),
    ('How do you stop a computer from getting too cold? You give it a byte.'),
    ('Why was the JavaScript developer sad? Because he didn''t "null" how to express himself.'),
    ('How many programmers does it take to change a light bulb? None, that''s a hardware problem.'),
    ('Why was the computer cold? It left its Windows open.'),
    ('How do you stop a computer from getting too cold? You give it a byte.'),
    ('Why did the computer go to therapy? It had too many bytes of emotional baggage.'),
    ('Why do programmers prefer dark mode? Light attracts bugs.'),
    ('How do you comfort a JavaScript bug? You console it.'),
    ('Why did the computer keep its drink on top of the modem? It wanted a data on the rocks.'),
    ('Why was the JavaScript developer sad? Because he didn''t "null" how to express himself.'),
    ('How many programmers does it take to change a light bulb? None, that''s a hardware problem.'),
    ('What''s a computer''s least favorite food? Spam.');

CREATE USER 'jokesuser'@'%' IDENTIFIED by 'J0k3sPWD!s';
GRANT ALL PRIVILEGES on jokes_db.* to 'jokesuser'@'%';
FLUSH PRIVILEGES;