const express = require('express');
const mysql = require('mysql');

const app = express();
const port = process.env.PORT || 3000;

// MySQL database connection configuration
const dbConfig = {
  host: process.env.DB_HOST || 'localhost',
  user: process.env.DB_USER || 'dbuser',
  password: process.env.DB_PASSWORD || 'DBP422W0rd',
  database: process.env.DB_DATABASE || 'jokes_db'
};

// Create a connection to the database
const connection = mysql.createConnection(dbConfig);

// Connect to MySQL
connection.connect(err => {
  if (err) {
    console.error('Error connecting to MySQL:', err);
    return;
  }
  console.log('Connected to MySQL');
});

// Serve HTML pages with buttons for fetching, adding, and canceling jokes

app.get('/', (req, res) => {
  renderJokesPage(res);
});

app.get('/add', (req, res) => {
  res.send(`
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Add Joke</title>
    </head>
    <body>
      <h1>Add Joke</h1>
      <textarea id="joke-input" rows="4" cols="50" placeholder="Enter your joke"></textarea>
      <br>
      <button onclick="addJoke()">Add</button>
      <button onclick="cancelAdd()">Cancel</button>

      <script>
        function addJoke() {
          const jokeInput = document.getElementById('joke-input').value;
          if (jokeInput.trim() !== '') {
            fetch('/add-joke', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({ joke: jokeInput })
            })
            .then(response => response.json())
            .then(data => {
              alert('Joke added successfully!');
              window.location.href = '/';
            })
            .catch(error => {
              console.error('Error adding joke:', error);
              alert('Failed to add joke. Please try again.');
            });
          }
        }

        function cancelAdd() {
          window.location.href = '/';
        }
      </script>
    </body>
    </html>
  `);
});

// API endpoint to add a joke to the database
app.post('/add-joke', express.json(), (req, res) => {
  const { joke } = req.body;

  if (joke.trim() !== '') {
    const query = 'INSERT INTO jokes_table (joke) VALUES (?)';

    connection.query(query, [joke], (err, results) => {
      if (err) {
        console.error('Error adding joke to the database:', err);
        res.status(500).json({ error: 'Internal Server Error' });
        return;
      }

      res.json({ success: true });
    });
  } else {
    res.status(400).json({ error: 'Joke cannot be empty' });
  }
});

app.get('/random-joke', (req, res) => {
  const query = 'SELECT joke FROM jokes_table ORDER BY RAND() LIMIT 1';

  connection.query(query, (err, results) => {
    if (err) {
      console.error('Error fetching random joke:', err);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }

    const joke = results[0] ? results[0].joke : 'No jokes available.';

    res.json({ joke });
  });
});

function renderJokesPage(res) {
  const query = 'SELECT joke FROM jokes_table ORDER BY RAND() LIMIT 1';

  connection.query(query, (err, results) => {
    if (err) {
      console.error('Error fetching random joke:', err);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }

    const joke = results[0] ? results[0].joke : 'No jokes available.';

    res.send(`
      <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Jokes App</title>
      </head>
      <body>
        <h1>Jokes App</h1>
        <div id="joke-container">${joke}</div>
        <button onclick="fetchRandomJoke()">Get Another Joke</button>
        <button onclick="navigateToAdd()">Add Joke</button>

        <script>
          async function fetchRandomJoke() {
            const response = await fetch('/random-joke');
            const jokeData = await response.json();
            document.getElementById('joke-container').innerText = jokeData.joke;
          }

          function navigateToAdd() {
            window.location.href = '/add';
          }
        </script>
      </body>
      </html>
    `);
  });
}

// Health check endpoint
app.get('/health', (req, res) => {
  res.status(200).send('OK');
});

// Start the Express server
app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
